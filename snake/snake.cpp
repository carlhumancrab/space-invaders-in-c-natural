#include "snake.hpp"






























SDL_Texture* Snake::createTexture(const char *path){SDL_Texture *newTexture = NULL
;SDL_Surface *loadedSurface = IMG_Load(path)

;if (loadedSurface == NULL)
{printf("Unable to load image %s! Error: %s\n", path, IMG_GetError())
;}else
{newTexture = SDL_CreateTextureFromSurface(rend, loadedSurface)
;if (newTexture == NULL)
{printf("Unable to create texture from %s! Error: %s\n", path, SDL_GetError())
;}SDL_FreeSurface(loadedSurface)

;}return newTexture

;}
void Snake::increaseBodySize(){snakeBodyTex.push_back(createTexture("images/white.png"))
;snakeBody.resize(score+1)
;SDL_QueryTexture(snakeBodyTex[score-1], NULL, NULL, &snakeBody[score-1].w, &snakeBody[score-1].h)
;snakeBody[score-1].w=control.w
;snakeBody[score-1].h=control.h
;if (score==1)
{snakeBody[score-1].x = control.x+control.w
;snakeBody[score-1].y = control.x+control.h
;}else
{snakeBody[score-1].x = snakeBody[score-2].x + control.w
;snakeBody[score-1].y =  snakeBody[score-2].y + control.h

;}}
void Snake::animation_loop(){{// animation loop
;}while (!close)
{SDL_Event event

;// Events mangement
;while (SDL_PollEvent(&event))
{switch (event.type)
{case SDL_QUIT:
{// handling of close button
;close = 1
;break
;}case SDL_KEYDOWN:
{// keyboard API for key pressed
;switch (event.key.keysym.scancode)
{case SDL_SCANCODE_A:
;case SDL_SCANCODE_LEFT:
{direction = 0
;break
;}case SDL_SCANCODE_D:
;case SDL_SCANCODE_RIGHT:
{direction = 1
;break
;}case SDL_SCANCODE_W:
;case SDL_SCANCODE_UP:
{direction = 2
;break
;}case SDL_SCANCODE_S:
;case SDL_SCANCODE_DOWN:
{direction = 3
;break
;}case SDL_SCANCODE_ESCAPE:
{close = 1
;break
;}default:
{break   

;}}}}}if (direction == 0)
{control.x -= speed
;if (score != 0)
{for (int i=0; i <= score; i++)
{snakeBody[i].x -= control.x + (control.w*(i+1)) 
;snakeBody[i].y = control.y
;}}if (control.x < 0)
{direction = 1
;}}else if (direction == 1)
{control.x += speed
;if (score != 0)
{for (int i=0; i <= score; i++)
{snakeBody[i].x += control.x + (control.w*(i+1))
;snakeBody[i].y = control.y
;}}if (control.x + control.w > 600)
{direction = 0
;}}else if (direction == 2)
{control.y -= speed
;if (score != 0)
{for (int i=0; i <= score; i++)
{snakeBody[i].y -= control.y + (control.h*(i+1)) 
;snakeBody[i].x = control.x
;}}if (control.y < 0)
{direction = 3
;}}else
{control.y += speed
;if (score != 0)
{for (int i=0; i <= score; i++)
{snakeBody[i].y += control.y + (control.h*(i+1)) 
;snakeBody[i].x = control.x

;}}if (control.y + control.h > 600)
{direction = 2

;}}if (control.y >= food.y && control.y <= food.y + food.w && control.x >= food.x && control.x <= food.x + food.h && control.x+control.h>=food.x && control.y+control.w>=food.y)
{score=score+1
;srand(time(0))
;food.x = rand() % 600-food.w
;food.y = rand() % 600-food.h
;increaseBodySize()

;}// clears the screen
;SDL_RenderClear(rend)
;//re-renders snake and food
;SDL_RenderCopy(rend, controlTex, NULL, &control)
;SDL_RenderCopy(rend, foodTex, NULL, &food)
;if (score!=0)
{for (int i=0; i < score; i++)
{SDL_RenderCopy(rend, snakeBodyTex[i], NULL, &snakeBody[i])
;}}// triggers the double buffers
;// for multiple rendering
;SDL_RenderPresent(rend)
;// calculates to 60 fps
;SDL_Delay(1000 / 60)
;if (score == 10)
{close = 1

;}}}
Snake::~Snake(){SDL_DestroyRenderer(rend)
;SDL_DestroyWindow(win)
;SDL_DestroyTexture(controlTex)

;}
Snake::Snake(){// returns zero on success else non-zero
;if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
{printf("error initializing SDL: %s\n", SDL_GetError())

;}win = SDL_CreateWindow("GAME", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 600, 600, 0)

;// triggers the program that controls
;// your graphics hardware and sets flags
;Uint32 render_flags = SDL_RENDERER_ACCELERATED
;// creates a renderer to render our images
;rend = SDL_CreateRenderer(win, -1, render_flags)
;controlTex = createTexture("images/red.png")
;foodTex = createTexture("images/blue.png")
;SDL_QueryTexture(controlTex, NULL, NULL, &control.w, &control.h)
;// food appears
;SDL_QueryTexture(foodTex, NULL, NULL, &food.w, &food.h)

;//randomise location
;food.w /= 20
;food.h /= 20

;srand(time(0))

;food.x = rand() % 600-food.w
;food.y = rand() % 600-food.h

;// adjust height and width of our image box.
;control.w /= 10
;control.h /= 10
;// sets initial x-position of object
;control.x = 300
;// sets initial y-position of object
;control.y = 300

;}
int main(){Snake snake
;snake.animation_loop()
;return 0
;}