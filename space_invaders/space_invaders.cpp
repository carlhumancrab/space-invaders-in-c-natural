#include "space_invaders.hpp"

SDL_Texture *SpaceInvaders::createTexture(const char *path)
{
    SDL_Texture *newTexture = NULL;
    SDL_Surface *loadedSurface = IMG_Load(path);
    if (loadedSurface == NULL)
    {
        printf("Unable to load image %s! Error: %s\n", path, IMG_GetError());
    }
    else
    {
        newTexture = SDL_CreateTextureFromSurface(rend, loadedSurface);
        if (newTexture == NULL)
            printf("Unable to create texture from %s! Error: %s\n", path, SDL_GetError());
        SDL_FreeSurface(loadedSurface);
    }
    return newTexture;
}
void SpaceInvaders::shoot(int origin)
{
    bulletTex = createTexture("images/blue.png");

    SDL_QueryTexture(bulletTex, NULL, NULL, &bullet.w, &bullet.h);

    // adjust height and width of our image box.
    bullet.w = 6;
    bullet.h = 6;

    // sets initial x-position of object
    bullet.x = origin;

    // sets initial y-position of object
    bullet.y = (600 - bullet.h);
}
void SpaceInvaders::animationLoop()
{
    // annimation loop
    while (!close)
    {
        SDL_Event event;

        // Events mangement
        while (SDL_PollEvent(&event))
        {
            switch (event.type)
            {

            case SDL_QUIT:
                // handling of close button
                close = 1;
                break;

            case SDL_KEYDOWN:
                // keyboard API for key pressed
                switch (event.key.keysym.scancode)
                {
                case SDL_SCANCODE_A:
                case SDL_SCANCODE_LEFT:
                    control.x -= speed / 30;
                    break;
                case SDL_SCANCODE_D:
                case SDL_SCANCODE_RIGHT:
                    control.x += speed / 30;
                    break;
                case SDL_SCANCODE_ESCAPE:
                    close = 1;
                    break;
                case SDL_SCANCODE_SPACE:
                    shoot(control.x);
                default:
                    break;
                }
            }
        }
        if (monster[9].x >= 570)
        {
            left = false;
        }
        else if (monster[0].x <= 0)
        {
            left = true;
        }
        for (int i = 0; i < numMonsters; i++)
        {
            if (left)
            {
                monster[i].x += 1;
            }
            else
            {
                monster[i].x -= 1;
            }
        }
        // right boundary
        if (control.x + control.w > 600)
            control.x = 600 - control.w;

        // left boundary
        if (control.x < 0)
            control.x = 0;

        // bottom boundary
        if (control.y + control.h > 600)
            control.y = 600 - control.h;

        // upper boundary
        if (control.y < 0)
            control.y = 0;

        bullet.y = bullet.y - speed / 50;

        if (bullet.y < 0)
        {
            SDL_DestroyTexture(bulletTex);
        }
        collisionCheck();
        // clears the screen
        SDL_RenderClear(rend);
        SDL_RenderCopy(rend, controlTex, NULL, &control);
        SDL_RenderCopy(rend, bulletTex, NULL, &bullet);
        for (int i = 0; i < numMonsters; i++)
        {
            SDL_RenderCopy(rend, monsterTex[i], NULL, &monster[i]);
        }
        // triggers the double buffers
        // for multiple rendering
        SDL_RenderPresent(rend);

        // calculates to 60 fps
        SDL_Delay(1000 / 60);
        if (score == 10)
        {
            close = 1;
        }
    }
}
SpaceInvaders::~SpaceInvaders()
{
    SDL_DestroyTexture(controlTex);
    for (int i = 0; i < numMonsters; i++)
    {
        SDL_DestroyTexture(monsterTex[i]);
    }
    SDL_DestroyTexture(bulletTex);
    SDL_DestroyRenderer(rend);
    SDL_DestroyWindow(win);
}
SpaceInvaders::SpaceInvaders()
{
    // retutns zero on success else non-zero
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
    {
        printf("error initializing SDL: %s\n", SDL_GetError());
    }
    win = SDL_CreateWindow("GAME", // creates a window
                           SDL_WINDOWPOS_CENTERED,
                           SDL_WINDOWPOS_CENTERED,
                           600, 600, 0);

    // triggers the program that controls
    // your graphics hardware and sets flags
    Uint32 render_flags = SDL_RENDERER_ACCELERATED;

    // creates a renderer to render our images
    rend = SDL_CreateRenderer(win, -1, render_flags);

    controlTex = createTexture("images/red.png");

    // connects our texture with dest to control position
    SDL_QueryTexture(controlTex, NULL, NULL, &control.w, &control.h);
    // adjust height and width of our image box.
    control.w /= 6;
    control.h /= 6;

    // sets initial x-position of object
    control.x = (600 - control.w) / 2;

    // sets initial y-position of object
    control.y = (600 - control.h);

    for (numMonsters = 0; numMonsters < 10; numMonsters++)
    {
        monsters();
    }
}
void SpaceInvaders::collisionCheck()
{
    int monCount = 0;
    for (auto mon : monster)
    {
        if (mon.x <= bullet.x + 30 && mon.x >= bullet.x - 30 && bullet.y <= mon.y + 30)
        {
            SDL_DestroyTexture(monsterTex[monCount]);
            SDL_DestroyTexture(bulletTex);
            bullet.x = 750;
            score++;
            break;
        }
        monCount++;
    }
}
void SpaceInvaders::monsters()
{
    monsterTex.push_back(createTexture("images/white.png"));
    monster.resize(numMonsters + 1);
    SDL_QueryTexture(monsterTex[numMonsters], NULL, NULL, &monster[numMonsters].w, &monster[numMonsters].h);

    monster[numMonsters].w = 30;
    monster[numMonsters].h = 30;

    monster[numMonsters].x = numMonsters * 2 * 30;
    monster[numMonsters].y = 30;
}
int main()
{
    SpaceInvaders spaceInvaders;
    spaceInvaders.animationLoop();

    return 0;
}
